#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QByteArray>

class TcpNotifyManager : public QObject
{
	Q_OBJECT

public:
	TcpNotifyManager(QObject *parent);
	virtual ~TcpNotifyManager();

public slots:
	void makeConnection();
	void detected();
	void recv();

private:
	QTcpServer* serv;
	QTcpSocket* clnt;

private:
	QByteArray makePacket(char opcode, char curFrameNo, char totalFrameNo, QByteArray data);
	QByteArray makeDetectedPacket();
	QByteArray makeStatusDataReplyPacket(char seqNum);
};
