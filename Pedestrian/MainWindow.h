#pragma once

#include <string>

#include <QtWidgets/QMainWindow>
#include <QThread>
#include <QString>
#include <QMessageBox>
#include <QTimer>
#include <QDateTime>
#include "ui_MainWindow.h"

#include <opencv2/highgui/highgui.hpp>

#include "WorkerThread.h"
#include "TcpNotifyManager.h"

Q_DECLARE_METATYPE(std::string)

using namespace cv;
using namespace std;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	virtual ~MainWindow();

private:
	Ui::MainWindowClass ui;
	QThread* thDetecting;
	QThread* thNetworking;
	TcpNotifyManager* notiManager;

	void setup();

signals:
	void sendSetup(QString videoName);
	void sendToggleStream();

private slots:
	void receiveOriginFrame(QImage frame);
	void receiveTrackingFrame(QImage frame);
	void receiveToggleStream();
	void receiveCrossMsg();
	void on_btnOpenVideo_clicked();
	void on_btnOpenServer_clicked();
};
