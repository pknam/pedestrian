#pragma once

#include <string>

#include <QObject>
#include <QMessageBox>
#include <opencv2/highgui/highgui.hpp>

#include "package_bgs/PBAS/PixelBasedAdaptiveSegmenter.h"
#include "package_tracking/BlobTracking.h"

using namespace std;
using namespace cv;

class WorkerThread : public QObject
{
	Q_OBJECT

public:
	WorkerThread(QObject *parent = 0);
	~WorkerThread();

private:
	IBGS *bgs;
	BlobTracking* blobTracking;

	Mat _frameOriginal;
	Mat _frameResized;
	Mat _frameTracking;
	VideoCapture* cap;

	bool status;
	bool toggleStream;

	bool bStop;
	bool initcmd;
	bool bgsStart;
	Mat img_blob;

	bool binaryThresholdEnable;
	int binaryThreshold;

	void checkIfDeviceAlreadyOpened(string videoName);
	void process();

signals:
	void sendOriginFrame(QImage frameProcessed);
	void sendTrackingFrame(QImage frameTracking);
	void sendCrossMsg();

public slots:
	void receiveGrabFrame();
	void receiveSetup(QString videoName);
	void receiveToggleStream();

	void startTracking();
};
