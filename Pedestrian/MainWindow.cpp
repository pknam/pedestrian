#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), thDetecting(nullptr), thNetworking(nullptr), notiManager(nullptr)
{
	ui.setupUi(this);

	Mat frame;
	Mat img_blob;

	setup();
}

MainWindow::~MainWindow()
{
	if (thDetecting)
		thDetecting->quit();

	if (thNetworking)
		thNetworking->quit();
}

void MainWindow::setup()
{
	thDetecting = new QThread();
	WorkerThread* worker = new WorkerThread();
	QTimer *workerTrigger = new QTimer();
	workerTrigger->setInterval(1);

	connect(workerTrigger, SIGNAL(timeout()), worker, SLOT(receiveGrabFrame()));
	connect(thDetecting, SIGNAL(finished()), worker, SLOT(deleteLater()));
	connect(thDetecting, SIGNAL(finished()), workerTrigger, SLOT(deleteLater()));
	connect(this, SIGNAL(sendSetup(QString)), worker, SLOT(receiveSetup(QString)));
	connect(worker, SIGNAL(sendOriginFrame(QImage)), this, SLOT(receiveOriginFrame(QImage)));
	connect(worker, SIGNAL(sendTrackingFrame(QImage)), this, SLOT(receiveTrackingFrame(QImage)));
	connect(worker, SIGNAL(sendCrossMsg()), this, SLOT(receiveCrossMsg()));
	connect(ui.btnStartTracking, SIGNAL(clicked()), worker, SLOT(startTracking()));
	connect(ui.btnPlayPause, SIGNAL(clicked()), worker, SLOT(receiveToggleStream()));

	workerTrigger->start();
	worker->moveToThread(thDetecting);
	workerTrigger->moveToThread(thDetecting);

	thDetecting->start();
}

void MainWindow::receiveOriginFrame(QImage frame)
{
	ui.imgOrigin->setPixmap(QPixmap::fromImage(frame));
}

void MainWindow::receiveTrackingFrame(QImage frame)
{
	ui.imgTracking->setPixmap(QPixmap::fromImage(frame));
}

void MainWindow::receiveToggleStream()
{
	//if (!ui->pushButtonPlay->text().compare(">")) ui->pushButtonPlay->setText("||");
	//else ui->pushButtonPlay->setText(">");

	//emit sendToggleStream();
}

void MainWindow::receiveCrossMsg()
{
	QDateTime curTime = QDateTime::currentDateTimeUtc();
	QString buf = curTime.toString();
	ui.txtLog->appendPlainText(buf + " Detected\n");
}

void MainWindow::on_btnOpenVideo_clicked()
{
	emit sendSetup(ui.txtVideoName->toPlainText());
}

void MainWindow::on_btnOpenServer_clicked()
{
	if (notiManager)
	{
		delete notiManager;
		delete thNetworking;
	}

	thNetworking = new QThread();
	notiManager = new TcpNotifyManager(this);

	connect(ui.btnDetected, SIGNAL(clicked()), notiManager, SLOT(detected()));

	notiManager->moveToThread(thNetworking);
	thNetworking->start();
}