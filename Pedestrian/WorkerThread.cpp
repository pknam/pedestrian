#include "WorkerThread.h"

WorkerThread::WorkerThread(QObject *parent)
	: QObject(parent), blobTracking(nullptr), bgs(nullptr), status(false),
	bStop(0), initcmd(0), bgsStart(0)
{
	if (blobTracking)
		delete blobTracking;
	if (bgs)
		delete bgs;

	bgs = new PixelBasedAdaptiveSegmenter;
	blobTracking = new BlobTracking;

	cap = new VideoCapture;
}

WorkerThread::~WorkerThread()
{
	if (blobTracking)
		delete blobTracking;

	if (bgs)
		delete bgs;

	if (cap->isOpened())
		cap->release();

	delete cap;
}

void WorkerThread::checkIfDeviceAlreadyOpened(string videoName)
{
	if (cap->isOpened())
		cap->release();
	cap->open(videoName);
}

void WorkerThread::process()
{
	Mat img_mask;
	Mat input_img = _frameResized.clone();

	if (bgsStart == 1)
	{
		bgs->process(input_img, img_mask, initcmd);
		//bgs->process(warp_img, img_mask, initcmd);

		if (!img_mask.empty())
		{
			// Perform blob tracking
			//blobTracking->process(warp_img, img_mask, img_blob);
			blobTracking->process(input_img, img_mask, img_blob);
			img_blob.copyTo(_frameTracking);
		}
	}
}

void WorkerThread::receiveGrabFrame()
{
	if (!toggleStream)
		return;

	(*cap) >> _frameOriginal;
	if (_frameOriginal.empty())
		return;

	resize(_frameOriginal, _frameResized, Size(640, 480));

	process();

	QImage originImage((const unsigned char *)_frameResized.data, _frameResized.cols, _frameResized.rows, QImage::Format_RGB888);
	QImage trackingImage((const unsigned char *)_frameTracking.data, _frameTracking.cols, _frameTracking.rows, QImage::Format_RGB888);
	emit sendOriginFrame(originImage);
	emit sendTrackingFrame(trackingImage);
}

void WorkerThread::receiveSetup(QString videoName)
{
	checkIfDeviceAlreadyOpened(videoName.toLocal8Bit().constData());
	if (!cap->isOpened())
	{
		status = false;
		return;
	}

	status = true;
	toggleStream = true;
}

void WorkerThread::receiveToggleStream()
{
	toggleStream = !toggleStream;
}

void WorkerThread::startTracking()
{
	bgsStart = 1;
}