#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <Windows.h>

#include "package_bgs/PBAS/PixelBasedAdaptiveSegmenter.h"
#include "package_tracking/BlobTracking.h"
#include "package_analysis/VehicleCouting.h"

#define KEY_ESC 27
#define KEY_SPACE 32
#define KEY_NEXT 'q'
#define KEY_CLEAR 'c'
#define KEY_START 's'


using namespace cv;
using namespace std;


int main(int argc, char **argv)
{
	//std::cout << "Using OpenCV " << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION << "." << CV_SUBMINOR_VERSION << std::endl;

	/* Open video file */
	CvCapture *capture = NULL;
	capture = cvCaptureFromAVI("C:\\Users\\pknam\\Downloads\\bandicam 2017-04-12 14-24-50-967.mp4");
	//capture = cvCaptureFromFile("rtsp://mpv.cdn3.bigCDN.com:554/bigCDN/definst/mp4:bigbuckbunnyiphone_400.mp4");
	//capture = cvCaptureFromAVI("dataset/video.avi");
	//capture = cvCaptureFromAVI("F:\\ExData\\video.avi");
	
	if (!capture) {
		std::cerr << "Cannot open video!" << std::endl;
		return 1;
	}

	/* Background Subtraction Algorithm */
	IBGS *bgs;
	bgs = new PixelBasedAdaptiveSegmenter;
	
	IplImage *frame;


	/* Homography */
	cv::Mat warp_img = Mat::zeros(200, 400, CV_8UC3);
	
	// roi point
	Point2f TopLeft = Point2f(38, 38);
	Point2f TopRight = Point2f(150, 10);
	Point2f BottomRight = Point2f(430, 180);
	Point2f BottomLeft = Point2f(100, 340);

	vector<Point2f> pts_src, pts_dst;

	// Set ROI point 
	pts_src.push_back(TopLeft);
	pts_src.push_back(TopRight);
	pts_src.push_back(BottomRight);
	pts_src.push_back(BottomLeft);

	// Set dst Image point (200*400)
	pts_dst.push_back(Point2f(0, 0));		// TopLeft
	pts_dst.push_back(Point2f(200, 0));		// TopRight
	pts_dst.push_back(Point2f(200, 400));       // BottomRight
	pts_dst.push_back(Point2f(0, 400)); // BottomLeft

	// find homography
	Mat h = cv::getPerspectiveTransform(pts_src, pts_dst);
	

	/* Blob Tracking Algorithm */
	cv::Mat img_blob;
	BlobTracking* blobTracking;
	blobTracking = new BlobTracking;

	/* Vehicle Counting Algorithm */
	VehicleCouting* vehicleCouting;
	vehicleCouting = new VehicleCouting;

	//std::cout << "Press 'q' to quit..." << std::endl;
	int key = 0;


	// performance FPS
	__int64 freq, start, finish;
	::QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);

	bool bStop = 0; // video stop flag
	bool initcmd = 0;
	bool bgsStart = 0;


	while (1)
	{
		frame = cvQueryFrame(capture);

		if (!frame) break;

		cv::Mat img_input(frame);

		// Just for measure time   
		::QueryPerformanceCounter((_LARGE_INTEGER*)&start);

		// Resize img
		cv::resize(img_input, img_input, cv::Size(320, 240));
		cv::imshow("Input", img_input);

		// warping
		//cv::warpPerspective(img_input, warp_img, h, cv::Size(200, 400));

		// bgs->process(...) internally process and show the foreground mask image
		cv::Mat img_mask;
		


		if (bgsStart == 1)
		{
			bgs->process(img_input, img_mask, initcmd);
			//bgs->process(warp_img, img_mask, initcmd);

			if (!img_mask.empty())
			{
				// Perform blob tracking
				//blobTracking->process(warp_img, img_mask, img_blob);
				blobTracking->process(img_input, img_mask, img_blob);

				// Perform vehicle counting
				//vehicleCouting->setInput(img_blob);
				//vehicleCouting->setTracks(blobTracking->getTracks());
				//vehicleCouting->process();
			}
		}
		// convert time into string
		::QueryPerformanceCounter((_LARGE_INTEGER*)&finish);
		char fps_str[20];
		double fps = freq / double(finish - start + 1);
		sprintf_s(fps_str, 20, "FPS: %.1lf", fps);
		printf("%s \n", fps_str);
		//cv::putText(frame, fps_str, cv::Point(5, 15), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 1);

		if (initcmd == 1)
			initcmd = 0;

		// video controller
		char ch = cv::waitKey(1);
		if (ch == KEY_ESC) break;				// ESC Key
		if (ch == KEY_SPACE) 	 				// SPACE Key
			bStop ^= 1;
		if (ch == KEY_CLEAR)
			initcmd = 1;
		if (ch == KEY_START)
			bgsStart = 1;
		if (bStop)
		{
			while ((ch = cv::waitKey(1)) != KEY_SPACE && ch != KEY_ESC && ch != KEY_NEXT);
			if (ch == KEY_ESC) break;
			if (ch == KEY_NEXT) continue;
			if (ch == KEY_SPACE)
				bStop ^= 1;
			if (ch == KEY_CLEAR)
				initcmd = 1;
			if (ch == KEY_START)
				bgsStart = 1;
		}


	}

	delete vehicleCouting;
	delete blobTracking;
	delete bgs;

	cvDestroyAllWindows();
	cvReleaseCapture(&capture);

	return 0;
}
