#include "TcpNotifyManager.h"

TcpNotifyManager::TcpNotifyManager(QObject *parent)
	: QObject(parent), serv(nullptr), clnt(nullptr)
{
	serv = new QTcpServer(this);
	connect(serv, SIGNAL(newConnection()), this, SLOT(makeConnection()));
	
	if (!serv->listen(QHostAddress::Any, 9000))
	{
		QMessageBox::critical((QWidget*)(this->parent()), "Error", "could not start server");
	}
	else
	{
		QMessageBox::information((QWidget*)(this->parent()), "Information", "Server started..");
	}
}

TcpNotifyManager::~TcpNotifyManager()
{
	if (serv)
	{
		serv->close();
		delete serv;
	}
}

void TcpNotifyManager::makeConnection()
{
	QTcpSocket* socket = serv->nextPendingConnection();

	// 소켓 하나만 유지
	if (clnt)
	{
		socket->close();
		return;
	}

	clnt = socket;
	connect(clnt, SIGNAL(readyRead()), this, SLOT(recv()));

	QMessageBox::information((QWidget*)(this->parent()), "Information", "clnt accepted");
}

void TcpNotifyManager::detected()
{
	if (this->clnt == nullptr)
		return;

	QMessageBox::information((QWidget*)(this->parent()), "Information", "detected");
	this->clnt->write(makeDetectedPacket());
	this->clnt->flush();
}

void TcpNotifyManager::recv()
{
	// 상태정보데이터(주기)에 대해서만 처리
	QByteArray buf = this->clnt->read(10);

	QByteArray replyData = makeStatusDataReplyPacket(buf.at(9));
	clnt->write(replyData);
}

QByteArray TcpNotifyManager::makePacket(char opcode, char curFrameNo, char totalFrameNo, QByteArray data)
{
	QByteArray packet;
	packet.append("HPDS");
	packet.append(opcode);
	packet.append(curFrameNo);
	packet.append(totalFrameNo);

	// data length as little-endian 2 bytes
	packet.append(data.length() & 0xff);
	packet.append((data.length() >> 8) & 0xff);

	packet.append(data);

	return packet;
}

QByteArray TcpNotifyManager::makeDetectedPacket()
{
	QByteArray packet_body;
	packet_body.append(1); // num of objects

	packet_body.append(1); // type of object

	packet_body.append(0xee); // left x
	packet_body.append((char)0);

	packet_body.append(0xee); // top y
	packet_body.append((char)0);

	packet_body.append(0xcc); // width
	packet_body.append((char)0);

	packet_body.append(0xcc); // height
	packet_body.append((char)0);

	return makePacket(1, 1, 1, packet_body);
}

QByteArray TcpNotifyManager::makeStatusDataReplyPacket(char seqNum)
{
	QByteArray packet_body;
	packet_body.append(seqNum);
	packet_body.append(1);	// normal(1) or abnormal(0)

	return makePacket(2, 1, 1, packet_body);
}